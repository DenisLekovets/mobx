import React from 'react';

import Dashboard from './dashboard'
import ThemeProvider from './providers/theme'
import {Provider} from 'mobx-react'
import RootStore from './stores/RootStore'

const rootStore = new RootStore();

const App = () => {
    return (
        <Provider counter1={rootStore.counter1} 
        counter2 ={rootStore.counter2}
        rootStore = {rootStore}>
        <ThemeProvider>
            <Dashboard />
        </ThemeProvider>
        </Provider>
    )
}


export default App