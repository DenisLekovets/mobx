import {observable, action, reaction, makeAutoObservable} from 'mobx'

class Counter{
    count = 0
    rootStore: any;
    
    constructor(rootStore){
        this.rootStore = rootStore;
        makeAutoObservable(this,{
            count: observable,
            increase: action,
            decrease: action
        })
        reaction(
            ()=>this.count,
            count => {
                console.log("Current count ", count)
            }
        )
        this.count = rootStore.initialCount;
    }

    increase = () =>{
        this.count+=1
    }

    decrease = () =>{
        this.count-=1
    }
}


export default Counter;