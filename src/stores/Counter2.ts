import {observable, action, reaction, makeAutoObservable} from 'mobx'

class Counter2{
    count = 0
    rootStore:any;
    constructor(rootStore){
        this.rootStore = rootStore;
        makeAutoObservable(this,{
            count: observable,
            increase: action,
            decrease: action
        })
        reaction(
            ()=>this.count,
            count => {
                console.log("Current count ", count)
            }
        )
    }

    increase = () =>{
        this.count+=1
    }

    decrease = () =>{
        this.count-=1
    }
}


export default Counter2;