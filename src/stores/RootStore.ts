import Counter from "./Counter";
import Counter2 from "./Counter2";

class RootStore{
    initialCount = 42;
    counter1: Counter;
    counter2: Counter2;
    constructor(){
        this.counter1 = new Counter(this)
        this.counter2 = new Counter2(this)
    }
}

export default RootStore